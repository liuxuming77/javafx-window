javafx 窗口装饰器

客户端一值不是java的强项,但javafx发布后,我们就应该开始改变这种观点了;ControlsFX、jfxtras等插件也让javafx如虎添翼.
javafx 提供了独立的UI开发工具JavaFX Scene Builder,配合强大智能的intellij idea ,让客户端开发更加简单,快捷.

感觉window窗口标题栏不是很美观,自制了一个窗口装饰器,对现有的代码几乎没有侵入.主要提供了窗口最大化、最小化、关闭、透明背景、背景颜色面板、圆角边框，暂没有实现可大小拖放。

使用方法:
1.- 将[javafx-window](https://git.oschina.net/panmingzhi/javafx-window.git) clone 到本地,使用mvn install进行打包,在项目中添加该依赖.或者直接将代码复制到项目中也可以

2.- 使用代码 StageDecorateTest.java
```
public class StageDecorateTest extends Application{

    public static void main(String[] args) {
        StageDecorateTest.launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(StageDecorateTest.class.getResource("form.fxml"));
        //只需切换下面一条setScene为StageDecorate.decorate()即可
        //primaryStage.setScene(new Scene(root)); 
        StageDecorate.decorate(primaryStage,root);
        primaryStage.setTitle("一卡通用户");
        primaryStage.show();
    }
}
```
2.- 项目截图：

![输入图片说明](http://git.oschina.net/uploads/images/2016/0130/135637_3388979e_87848.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0130/135653_3c4472a2_87848.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0130/135707_4053279f_87848.png "在这里输入图片标题")