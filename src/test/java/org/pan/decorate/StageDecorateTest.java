package org.pan.decorate;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;

/**
 * Created by xiaopan on 2016-01-30.
 */
public class StageDecorateTest extends Application{

    public static void main(String[] args) {
        StageDecorateTest.launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(StageDecorateTest.class.getResource("form.fxml"));
        StageDecorate.decorate(primaryStage,root);
        primaryStage.setTitle("一卡通用户");
        primaryStage.show();
    }
}