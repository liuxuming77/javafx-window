package org.pan.decorate.alert;

import javafx.application.Application;
import javafx.stage.Stage;
import org.junit.Ignore;
import org.junit.Test;
import org.pan.alert.AlertType;
import org.pan.alert.Alerts;

/**
 * Created by Administrator on 2016/2/2.
 */
public class AlertsTest extends Application{


    @Override
    public void start(Stage primaryStage) throws Exception {
        showInfo();
    }

    @Ignore
    @Test
    public void showInfo(){
        Alerts.create(AlertType.INFO).title("提示").message("这是一个提示信息").show();
    }

}