package org.pan.alert;

/**
 * Created by Administrator on 2016/2/2.
 */
public class ButtonType {

    static ButtonType yes = new ButtonType("确定");
    static ButtonType no = new ButtonType("取消");

    private String text;

    public ButtonType(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
