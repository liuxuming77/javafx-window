package org.pan.decorate;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.effect.Light;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.pan.StageDrager;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * 自定义的stage控制器,用于操作最大化,最小化,关闭,颜色,窗口拖动功能
 * Created by xiaopan on 2016-01-30.
 */
public class StageController{


    private Stage stage;
    private Light.Point point = new Light.Point();

    @FXML
    public ColorPicker colorPicker;
    @FXML
    public HBox banner;
    @FXML
    public VBox stageRoot;
    @FXML
    public Label title;

    public void setStage(Stage stage) {
        this.stage = stage;
        new StageDrager(this.stage).bindDrag(banner);
        this.title.textProperty().bindBidirectional(stage.titleProperty());
    }

    public void addContent(Parent content) {
        this.stageRoot.getChildren().add(content);
        VBox.setVgrow(content, Priority.ALWAYS);
    }

    public void min() {
        stage.setIconified(true);
    }

    public void max() {
        stage.setMaximized(!stage.isMaximized());
    }

    public void close() {
        stage.close();
    }

    public void color() {
        colorPicker.show();
    }

    public void changeColor(ActionEvent event) {
        ColorPicker colorPicker = (ColorPicker) event.getSource();
        String rgbString = "#" + Integer.toHexString(colorPicker.getValue().hashCode());
        stageRoot.setStyle(String.format("-fx-background-color:%s;",rgbString));
    }
}
